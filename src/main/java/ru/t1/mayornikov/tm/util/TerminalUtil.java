package ru.t1.mayornikov.tm.util;

import java.io.IOException;
import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = nextLine();
        Integer number = null;
        try {
            number = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            number = 0;
        }
        return number;
    }

}
