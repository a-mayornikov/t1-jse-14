package ru.t1.mayornikov.tm.api.controller;

public interface ICommandController {

    void initDemoData();

    void showErrorArgument();

    void showSystemInfo();

    void showErrorCommand();

    void showVersion();

    void showAbout();

    void showHelp();

}
