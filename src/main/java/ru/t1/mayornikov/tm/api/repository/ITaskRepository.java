package ru.t1.mayornikov.tm.api.repository;

import ru.t1.mayornikov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    boolean existsById(String id);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);

    Task create(String name);

    Task findOne(String id);

    Task findOne(Integer index);

    Task remove(Task task);

    Task remove(String id);

    Task remove(Integer index);

    Integer getSize();

}