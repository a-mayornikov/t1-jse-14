package ru.t1.mayornikov.tm.controller;

import ru.t1.mayornikov.tm.api.controller.ITaskController;
import ru.t1.mayornikov.tm.api.service.ITaskService;
import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Task;
import ru.t1.mayornikov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeTaskStatus(id, status);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeTaskStatus(index, status);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatus(id, Status.IN_PROGRESS);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeTaskStatus(index, Status.IN_PROGRESS);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void completeTaskById() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatus(id, Status.COMPLETED);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeTaskStatus(index, Status.COMPLETED);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOW TASKS]");
        renderTasks();
    }

    @Override
    public void showTasksSorted() {
        System.out.println("[SORTED TASKS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final Sort sort = Sort.toSort(TerminalUtil.nextLine());
        renderTasks(taskService.findAll(sort));
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TO DO:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[DONE]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.remove(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.remove(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOne(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[DONE]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOne(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[DONE]");
    }

    @Override
    public void showTaskByProjectId() {
       System.out.println("[TASK LIST BY PROJECT ID]");
       System.out.println("ENTER PROJECT ID:");
       final String projectId = TerminalUtil.nextLine();
       final List<Task> tasks = taskService.findAllByProjectId(projectId);
       renderTasks(tasks);
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOne(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TO DO:");
        final String description = TerminalUtil.nextLine();
        task.setName(name);
        task.setDescription(description);
        System.out.println("[DONE]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOne(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TO DO:");
        final String description = TerminalUtil.nextLine();
        task.setName(name);
        task.setDescription(description);
        System.out.println("[DONE]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        final String id = task.getId();
        final String projectId = task.getProjectId();
        final String name = task.getName();
        final String description = task.getDescription();
        final String status = Status.toName(task.getStatus());
        if (!"".equals(id)) System.out.println("ID: " + id);
        if (projectId != null) System.out.println("PROJECT ID: " + projectId);
        if (!"".equals(name)) System.out.println("NAME: " + name);
        if (!"".equals(description)) System.out.println("TO DO: " + description);
        if (!"".equals(status)) System.out.println("STATUS: " + status);
    }

    private void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (Task task : tasks) {
            if (task == null) continue;
            System.out.println(index++ + ".");
            showTask(task);
        }
        if (index == 1) System.out.println("No one task found... ");
    }

    private void renderTasks() {
        int index = 1;
        for(final Task task : taskService.findAll()) {
            if (task == null) continue;
            System.out.println(index++ + ".");
            showTask(task);
        }
        if (index == 1) System.out.println("No one task found... ");
    }

}